#Azure point to site on a Mac

#create RG
az group create -n vpnet

#create network and subnet
az network vnet create -g vpnet -n VNet1 --address-prefixes 192.168.0.0/16 --subnet-name FrontEnd --subnet-prefix 192.168.1.0/24
az network vnet subnet create -n GatewaySubnet --address-prefix 192.168.200.0/24 --vnet-name VNet1 -g vpnet

#create a public ip
az network public-ip create -n vnet1gw-pubip -g vpnet --dns-name vnet1gwvpn

#Creat VPN Gateway - takes 20 min
az network vnet-gateway create  -n VNet1GW -g vpnet --public-ip-addresses vnet1gw-pubip --sku VpnGw1 --address-prefixes 172.16.201.0/24 --client-protocol IkeV2 --vnet VNet1 --no-wait


#Create key and rootCA on your mac

openssl genrsa -out rootCA.key 2048
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem

#Create device key, request and sign it with rootCA

openssl genrsa -out device.key 2048
openssl req -new -key device.key -out device.csr
openssl x509 -req -in device.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out device.crt -days 500 -sha256

#Convert to pfx and add to keychain, with password

openssl pkcs12 -export -out device.pfx -inkey device.key -in device.crt -certfile rootCA.pem
open device.pfx

#add rootCA to keychain
open rootCA.pem


#base64 encode the rootCA and add it to Virtual Network Gateway -> Point-to-site configuration
cat rootCA.pem | base64 -|pbcopy

#download the VPN client configuration, add the Generic/VpnServerRoot.cer to Keychain, then look at VpnSettings.xml

#Create an IKEv2 VPN, use the public DNS name of the VPN gateway for both server address and RemoteID. Open Authentication Settings and use the certificate.

@Connect and go! 


References:
https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-howto-point-to-site-resource-manager-portal

https://medium.com/@arisplakias/azure-point-to-site-vpn-certificates-with-openssl-406838731a7c

http://larryn.blogspot.nl/2017/03/openssl-and-azure-vpn.html